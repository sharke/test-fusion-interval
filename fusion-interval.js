const foo = (matrice) => {
  if (matrice.length === 0 || matrice[0].length < 2) {
    console.log(" format d 'entree incorect");
    return [];
  }

  // trier la matrice par les min du plus petit min au plus grand min !
  const matriceSorted = matrice.sort((a, b) => a[0] - b[0]);

  // initialiser le resultat par le 1er element de la matrice
  let resFusion = [matriceSorted[0]];

  // fusionner les interval
  for (i = 1; i < matriceSorted.length; i++) {
    // si interval(n) inclue interval(n+1) en garde  interval qui inclue l autre cad interval(n)
    if (resFusion[resFusion.length - 1][1] >= matriceSorted[i][1]) {
      continue;
    } else {
      // si max de interval(n) >= le min de interval(n+1) en fusionne  de la sort [ min de interval(n) , max de interval(n+1)]
      if (resFusion[resFusion.length - 1][1] >= matriceSorted[i][0]) {
        resFusion = [[resFusion[resFusion.length - 1][0], matriceSorted[i][1]]];
      } else {
        // si max de interval(n) < le min de interval(n+1) en ajoute interval(n+1) a l interval(n) dans le resultat !!
        resFusion = [...resFusion, matriceSorted[i]];
      }
    }
  }
  return resFusion;
};


// testing de foo 
console.log("-1) [[5],[]]", foo([[5], []]));
console.log("0) []", foo([]));
console.log(
  "1) fusion du [[0, 3], [6, 10]] : ",
  foo([
    [0, 3],
    [6, 10],
  ])
); // expected [[0, 3], [6, 10]]
console.log(
  "2) fusion du [[0, 5], [3, 10]]  : ",
  foo([
    [0, 5],
    [3, 10],
  ])
); // expected [[0, 10]]
console.log(
  "3) fusion du ([[0, 5], [2, 4]] : ",
  foo([
    [0, 5],
    [2, 4],
  ])
); // expected [[0, 5]]
console.log(
  "4) fusion du [[7, 8], [3, 6], [2, 4]] : ",
  foo([
    [7, 8],
    [3, 6],
    [2, 4],
  ])
); // expected [[2, 6], [7, 8]]
console.log(
  "5) fusion du [[3, 6], [3, 4], [15, 20], [16, 17], [1, 4], [6, 10], [3, 6]] : ",
  foo([
    [3, 6],
    [3, 4],
    [15, 20],
    [16, 17],
    [1, 4],
    [6, 10],
    [3, 6],
  ])
); // expected [[1, 10], [15, 20]]
